:: Iniciamos el servidor de streaming
start rtsp-simple-server %RTSP_SIMPLE_SERVER%\rtsp-simple-server.yml
:: Metemos una pausa de 3 segundos para esperar que el servidor se prepare para recibir las fuentes de video
ping 127.0.0.1 -n 3 > nul
:: Enviamos al servidor de streaming las fuentes de video y rutas por donde se podrá visualizar
start ffmpeg -re -stream_loop -1 -i cam1.mp4 -c copy -crf 0 -max_muxing_queue_size 9999 -f rtsp rtsp://localhost:8554/videoGrabado/1 
start ffmpeg -re -stream_loop -1 -i cam2.mp4 -c copy -crf 0 -max_muxing_queue_size 9999 -f rtsp rtsp://localhost:8554/videoGrabado/2